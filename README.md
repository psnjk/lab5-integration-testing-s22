# Lab5 -- Integration testing

## Homework

As a homework you will need to develop BVA and Decision table for your service, guaranteed that your service has a bug somewhere(maybe even few), you need to catch'em all using BVA. Submit your catched bugs and Tables, adding them to the Readme of your branch. 

### Response

https://script.google.com/macros/s/AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w/exec?service=getSpec&email=d.muhutdinov@innopolis.university

Here is InnoCar Specs:
Budet car price per minute = 25
Luxury car price per minute = 56
Fixed price per km = 11
Allowed deviations in % = 10
Inno discount in % = 14.000000000000002

### BVA

| Parameter          | Equivalence Classes|
| ------------------ | ----------------------------------- |
| `Type`         | `budget`, `luxury`, `nonsense`|
| `Plan` | `minute`, `fixed_price`, `nonsense`|
| `Distance`             | `<=0`, `>0`|
| `Planed Distance`     | `<=0`, `>0`|
| `Time`             | `<=0`, `>0`|
| `Planed time`             | `<=0`, `>0`|
| `InnoDiscount`    | `yes`, `no`, `nonsense`|

### Decision Table

| Conditions (inputs) | Values                              |  R1   |  R2   |  R3   |  R4   |     R5     |     R6     |     R7     |      R8       |    R9    |      R10      |   R11    |   R12    |      R13      |   R14    |
|---------------------|-------------------------------------|:-----:|:-----:|:-----:|:-----:|:----------:|:----------:|:----------:|:-------------:|:--------:|:-------------:|:--------:|:--------:|:-------------:|:--------:|
| `distance`          | `<=0`, `>0`                         | `<=0` | `>0`  | `>0`  | `>0`  |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |     `>0`      |   `>0`   |   `>0`   |     `>0`      |   `>0`   |
| `planned_distance`  | `<=0`, `>0`                         |   *   | `<=0` | `>0`  | `>0`  |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |     `>0`      |   `>0`   |   `>0`   |     `>0`      |   `>0`   |
| `time`              | `<=0`, `>0`                         |   *   |   *   | `<=0` | `>0`  |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |     `>0`      |   `>0`   |   `>0`   |     `>0`      |   `>0`   |
| `planned_time`      | `<=0`, `>0`                         |   *   |   *   |   *   | `<=0` |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |     `>0`      |   `>0`   |   `>0`   |     `>0`      |   `>0`   |
| `type`              | `budget`, `luxury`, `nonsense`      |   *   |   *   |   *   |   *   | `nonsense` |     *      |     *      |   `luxury`    | `budget` |   `budget`    | `luxury` | `budget` |   `budget`    | `luxury` |
| `plan`              | `minute`, `fixed_price`, `nonsense` |   *   |   *   |   *   |   *   |     *      | `nonsense` |     *      | `fixed_price` | `minute` | `fixed_price` | `minute` | `minute` | `fixed_price` | `minute` |
| `inno_discount`     | `yes`, `no`, `nonsense`             |   *   |   *   |   *   |   *   |     *      |     *      | `nonsense` |       *       |   `no`   |     `no`      |   `no`   |  `yes`   |     `yes`     |  `yes`   |
| **Results**         |                                     |       |       |       |       |            |            |            |               |          |               |          |          |               |          |
| Invalid Request     |                                     |   X   |   X   |   X   |   X   |     X      |     X      |     X      |       X       |          |               |          |          |               |          |
| 200                 |                                     |       |       |       |       |            |            |            |               |    X     |       X       |    X     |    X     |       X       |    X     |


### Test Cases

| ID  | Desition Table Entry | `distance` | `planned_distance` | `time` | `planned_time` | `type`     | `plan`        | `inno_discount` | Expected Result     | Actual Result   |
| --- | -------------------- | ---------- | ------------------ | ------ | -------------- | ---------- | ------------- | --------------- | ------------------- | --------------- |
| 0   | R1                   | -10        | 1                  | 1      | 1              | `budget`   | `minute`      | `yes`           | Invalid Request     | Invalid Request |
| 1   | R1                   | 0          | 1                  | 1      | 1              | `budget`   | `minute`      | `yes`           | **Invalid Request** | **21.5**        |
| 2   | R2                   | 1          | -10                | 1      | 1              | `budget`   | `minute`      | `yes`           | Invalid Request     | Invalid Request |
| 3   | R2                   | 1          | 0                  | 1      | 1              | `budget`   | `minute`      | `yes`           | **Invalid Request** | **21.5**        |
| 4   | R3                   | 1          | 1                  | -10    | 1              | `budget`   | `minute`      | `yes`           | Invalid Request     | Invalid Request |
| 5   | R3                   | 1          | 1                  | 0      | 1              | `budget`   | `minute`      | `yes`           | **Invalid Request** | **0**           |
| 6   | R4                   | 1          | 1                  | 1      | -10            | `budget`   | `minute`      | `yes`           | Invalid Request     | Invalid Request |
| 7   | R4                   | 1          | 1                  | 1      | 0              | `budget`   | `minute`      | `yes`           | **Invalid Request** | **21.5**        |
| 8   | R5                   | 1          | 1                  | 1      | 1              | `nonsense` | `minute`      | `yes`           | Invalid Request     | Invalid Request |
| 9   | R6                   | 1          | 1                  | 1      | 1              | `budget`   | `nonsense`    | `yes`           | Invalid Request     | Invalid Request |
| 10  | R7                   | 1          | 1                  | 1      | 1              | `budget`   | `minute`      | `nonsense`      | Invalid Request     | **25**          |
| 11  | R11                  | 1          | 1                  | 1      | 1              | `luxury`   | `fixed_price` | `yes`           | Invalid Request     | Invalid Request |
| 12  | R8                   | 1          | 1                  | 1      | 1              | `budget`   | `minute`      | `yes`           | 21.5                | 21.5            |
| 13  | R8                   | 1          | 1                  | 1000   | 1              | `budget`   | `minute`      | `yes`           | 21500               | 21500           |
| 14  | R9                   | 1          | 1                  | 1      | 1              | `budget`   | `fixed_price` | `yes`           | **9.46**            | **10.75**       |
| 15  | R9                   | 2          | 1                  | 1      | 1              | `budget`   | `fixed_price` | `yes`           | **18.92**           | **14.333333333**|
| 16  | R9                   | 1          | 1                  | 2      | 1              | `budget`   | `fixed_price` | `yes`           | **9.46**            | **28.666666666**|
| 17  | R9                   | 2          | 1                  | 2      | 1              | `budget`   | `fixed_price` | `yes`           | **18.92**           | **28.666666666**|
| 18  | R12                  | 1          | 1                  | 1      | 1              | `budget`   | `minute`      | `no`            | 25                  | 25              |
| 19  | R12                  | 1          | 1                  | 1000   | 1              | `budget`   | `minute`      | `no`            | 25000               | 25000           |
| 20  | R13                  | 2          | 1                  | 1      | 1              | `budget`   | `fixed_price` | `no`            | **22**              | **16.666666666**|
| 21  | R13                  | 1          | 1                  | 2      | 1              | `budget`   | `fixed_price` | `no`            | **11**              | **33.333333333**|
| 22  | R13                  | 2          | 1                  | 2      | 1              | `budget`   | `fixed_price` | `no`            | **22**              | **33.333333333**|
| 23  | R13                  | 1          | 1                  | 1      | 1              | `budget`   | `fixed_price` | `no`            | **11**              | **12.5**        |
| 24  | R10                  | 1          | 1                  | 1      | 1              | `luxury`   | `minute`      | `yes`           | 48.16               | 48.16           |
| 25  | R10                  | 1          | 1                  | 1000   | 1              | `luxury`   | `minute`      | `yes`           | 48160               | 48160           |
| 26  | R14                  | 1          | 1                  | 1      | 1              | `luxury`   | `minute`      | `no`            | 56                  | 56              |
| 27  | R14                  | 1          | 1                  | 1000   | 1              | `luxury`   | `minute`      | `no`            | 56000               | 56000           |

### Bugs
All bugs are highlighted in bold in the table above.
Detected bugs:
- 0 distance does not return zero or Invalid Request result
- 0 planned_distance does not return zero or Invalid Request result
- 0 time does not return Invalid Request result
- 0 planned_time does not return zero or Invalid Request result
- nonsense value for inno_discount appears as the "no" input, and does not return Invalid Request result
- budget type when combined with fixed_price plan computes the value incorrectly. It is noticeable that the time parameter takes part in the calculation of the value.
